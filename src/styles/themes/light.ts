import { extendTheme } from "@chakra-ui/react";

const theme = extendTheme({
  title: "light",
  colors: {
    primary: "#f7fafc", //f7fafc
    secondary: "#1a202c",
    tertiary: "#808080",
  },
});

export default theme;
